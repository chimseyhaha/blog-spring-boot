package com.korean.blogs.posts.service.impl;

import com.korean.blogs.posts.mapper.PostMapper;
import com.korean.blogs.posts.vo.PostsVO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {

    private final PostMapper postMapper;

    public PostService(PostMapper postMapper) {
        this.postMapper = postMapper;
    }

    public List<PostsVO> findAll() {
        return postMapper.findAll();
    }
}
