package com.korean.blogs.posts.mapper;

import com.korean.blogs.posts.vo.PostsVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PostMapper {
    List<PostsVO> findAll();
}
