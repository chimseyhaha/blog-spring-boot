package com.korean.blogs.posts.controller;

import com.korean.blogs.posts.service.impl.PostService;
import com.korean.blogs.posts.vo.PostsVO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
@RequestMapping("/posts")
public class PostsController {

    private final PostService postService;

    public PostsController(PostService postService) {
        this.postService = postService;
    }


    @RequestMapping("/")
    public String getAllPosts(Model model) {
        List<PostsVO> posts = postService.findAll();
        System.out.println(posts);
        model.addAttribute("posts", posts);
        return "posts/list";
    }
}
