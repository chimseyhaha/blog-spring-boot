package com.korean.blogs.posts.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PostsVO {
    private Integer id;
    private String title;
    private String contents;
    private String reg_dt;
    private String upd_dt;
}
